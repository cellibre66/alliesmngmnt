package com.management.alliesmngmnt;

/**
 *
 * @Mike Johnson
 *
 * Employees (Allies) Register Software wich works using SpringBoot Framewor (REST/API).
 * -Backend (SpringBoot FrameWork)
 * -Backend = Connected to DataBase Using JDBC
 * -PostMan = Consume BackEnd EndPoints
 * -Dependencies Used SpringWeb - SpringData - PostgreSQL Driver - Validation - Lombok
 *
 *
 * Structured in a traditional layered architecture.
 * With the following characteristics:
 *
 * -The top layer can only access the layer
 * lower
 *  -The cross cording layer accesses the other layers
 *
 * -the web layer includes controllers and handling
 * exceptions for validation
 *
 *  -domain package includes entity and service management
 *
 * -the persistence package contains the repository package
 *
 * -spring JPA Helps to make CRUD requests to our database without using SQL (structured query language)
 * The common layer contains DTO (Data Transfer Option) packet
 *
 *
 * Proyect Contains Category and Allies Entities
 * 	Category remains on the grounds of be used as Different Management Options.
 * 		The Main and FisrtOne Option Now is the required
 * 		EMPLOYEE("Allies") REGISTRATION
 * 	***But The Main Reason to define a Category Entity is Provide Posibility to
 * 	   use software for different Corp needed such as Employee Paying or Attendance Administration
 *
 *
 * 	Allies Entity is defined to Automatically save and manage specific Employee("Allies")information
 * 	such as Name, Middle Name, Last Names and so On , so Forth.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlliesMngmntApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlliesMngmntApplication.class, args);
    }

}
