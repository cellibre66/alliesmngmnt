package com.management.alliesmngmnt.web.control;

import com.management.alliesmngmnt.common.dto.AlliesDto;
import com.management.alliesmngmnt.domain.entity.Allies;
import com.management.alliesmngmnt.domain.entity.service.AlliesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/allies")
@RequiredArgsConstructor

public class AlliesController {
    private final AlliesService alliesService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Allies>> getAllAllies(){
        return new ResponseEntity<>(alliesService.findAllAllies(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Allies> getAlliesById(@PathVariable("id") Long id){
        return new ResponseEntity<>(alliesService.findAlliesById(id), HttpStatus.OK);
    }


    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Allies> createAllies(@Validated @RequestBody AlliesDto alliesDto){
        return new ResponseEntity<>(alliesService.createAllies(alliesDto), HttpStatus.OK);

    }

    @PutMapping(consumes = "application/json", produces = "application/json")
    public  ResponseEntity<Allies> updateAllies(@Validated @RequestBody AlliesDto alliesDto){
        return new ResponseEntity<>(alliesService.updateAllies(alliesDto), HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity deleteAlliesById(@PathVariable("id") Long id){
        alliesService.disableAlliesById(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }




}


