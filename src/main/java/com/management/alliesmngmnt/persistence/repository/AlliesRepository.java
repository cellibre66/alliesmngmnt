package com.management.alliesmngmnt.persistence.repository;

import com.management.alliesmngmnt.domain.entity.Allies;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlliesRepository extends CrudRepository<Allies, Long>{
    Allies findAlliesByIdAndDeleted(Long id, Boolean deleted);
}
