package com.management.alliesmngmnt.common.dto;

import com.management.alliesmngmnt.domain.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class AlliesDto {
    private Long id;
    private String name;
    private String name2;
    private String lastName;
    private String lastName2;
    private String country;
    private Short typeIDDoc;
    private Short idNumb;
    private String email;
    private Date hiringDate;
    private String department;
    private Boolean status;
    private Boolean deleted;
    private Date registrationTime;
    private Category category;
    private Short categoryId;
}
