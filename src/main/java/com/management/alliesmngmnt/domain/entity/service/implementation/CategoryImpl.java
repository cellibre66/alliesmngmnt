package com.management.alliesmngmnt.domain.entity.service.implementation;

import com.management.alliesmngmnt.common.dto.CategoryDto;
import com.management.alliesmngmnt.domain.entity.Category;
import com.management.alliesmngmnt.domain.entity.service.CategoryService;
import com.management.alliesmngmnt.persistence.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    private void catExist(String name){
        if(categoryRepository.existsByName(name)){
            throw new DuplicateKeyException("category Name Already Exist");
        }


    }


    @Override
    public List<Category> findAllCategories() {
        return (List<Category>) categoryRepository.findAll();

    }

    @Override
    public Category findCategoryById(short id) {
        Category category = categoryRepository.findCategoryById(id);
        if(category == null){
            throw new EntityNotFoundException("Category with Id " + id + "Is Not Defined");
        }
        return category;
    }

    @Override
    public Category createCategory(CategoryDto categoryDto) {
        Category category =  new Category();
        this.catExist(categoryDto.getName());
        BeanUtils.copyProperties(categoryDto, category);

        return categoryRepository.save(category);
    }

    @Override
    public Category updateCategory(CategoryDto categoryDto) {
        Category category = new Category();
        BeanUtils.copyProperties(categoryDto, category);
        return categoryRepository.save(category);

    }

    @Override
    public void deleteCategoryById(short id) {
        this.findCategoryById(id);
        categoryRepository.deleteById(id);

    }
}
