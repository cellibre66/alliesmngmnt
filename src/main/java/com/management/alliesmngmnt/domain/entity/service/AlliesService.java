package com.management.alliesmngmnt.domain.entity.service;

import com.management.alliesmngmnt.common.dto.AlliesDto;
import com.management.alliesmngmnt.domain.entity.Allies;

import java.util.List;

public interface AlliesService {
    List<Allies> findAllAllies();
    Allies findAlliesById(long id);

    Allies createAllies(AlliesDto alliesDto);
    Allies updateAllies(AlliesDto alliesDto);

    void disableAlliesById(Long id);
}
