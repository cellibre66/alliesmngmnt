package com.management.alliesmngmnt.domain.entity;

import lombok.*;

import java.time.LocalDateTime;
import java.util.Map;

@AllArgsConstructor @NoArgsConstructor @Getter @Setter @Builder
public class ExceptionResponseDto {
    private LocalDateTime timeStamp;
    private String error;
    private Map<String, ?> message;
}
