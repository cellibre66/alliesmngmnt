package com.management.alliesmngmnt.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "categories")
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class Category {
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "allies_id_seq")
    @SequenceGenerator(name = "allies_id_seq", initialValue = 1, allocationSize = 1)
    private short id;
    @Column(nullable = false, length = 50)
    private String name;

}

