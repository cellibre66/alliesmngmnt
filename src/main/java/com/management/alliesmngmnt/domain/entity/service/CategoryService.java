package com.management.alliesmngmnt.domain.entity.service;

import com.management.alliesmngmnt.common.dto.CategoryDto;
import com.management.alliesmngmnt.domain.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAllCategories();
    Category findCategoryById(short id);
    Category createCategory(CategoryDto categoryDto);
    Category updateCategory(CategoryDto categoryDto);
    void deleteCategoryById(short id);

}
