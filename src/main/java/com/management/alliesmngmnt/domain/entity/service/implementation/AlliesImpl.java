package com.management.alliesmngmnt.domain.entity.service.implementation;

import com.management.alliesmngmnt.common.dto.AlliesDto;
import com.management.alliesmngmnt.domain.entity.Allies;
import com.management.alliesmngmnt.domain.entity.service.AlliesService;
import com.management.alliesmngmnt.domain.entity.service.CategoryService;
import com.management.alliesmngmnt.persistence.repository.AlliesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AlliesImpl implements AlliesService {
    private final AlliesRepository alliesRepository;
    private final CategoryService categoryService;


    @Override
    public List<Allies> findAllAllies() {
        return (List<Allies>) alliesRepository.findAll();
    }

    @Override
    public Allies findAlliesById(long id) {
        Allies allies = alliesRepository.findAlliesByIdAndDeleted(id, false);
        if (allies == null){
            throw new EntityNotFoundException("The Allie with id" + id + "was not Found");
        }
        return allies;
    }



    @Override
    public Allies createAllies(AlliesDto alliesDto) {
        Allies allies = new Allies();
        allies.setCategory(categoryService.findCategoryById(alliesDto.getCategoryId()));
        BeanUtils.copyProperties(alliesDto, allies);
        allies.setDeleted(false);
        return alliesRepository.save(allies);
    }

    @Override
    public Allies updateAllies(AlliesDto alliesDto) {
        Allies allies = new Allies();
        BeanUtils.copyProperties(alliesDto, allies);
        return alliesRepository.save(allies);

    }



    @Override
    public void disableAlliesById(Long id) {
        Allies allies = findAlliesById(id);
        allies.setDeleted(true);
        alliesRepository.save(allies);

    }


}
