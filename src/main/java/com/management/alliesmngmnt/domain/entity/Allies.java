package com.management.alliesmngmnt.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "allies")
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class Allies {
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categories_id_seq")
    @SequenceGenerator(name = "categories_id_seq", sequenceName = "categories_id_seq", initialValue = 1, allocationSize = 1)
    private Long id;
    @Column(nullable = false, length = 20)
    private String name;
    @Column(nullable = false, length = 50)
    private String name2;
    @Column(nullable = false, length = 20)
    private String lastName;
    @Column(nullable = false, length = 20)
    private String lastName2;
    @Column(nullable = false)
    private String country;
    @Column(nullable = false, length = 10)
    private Short typeIDDoc;
    @Column(nullable = false, length = 20)
    private Short idNumb;
    @Column(nullable = false, length = 250)
    private String email;
    @Column(nullable = false)
    private Date hiringDate;
    @Column(nullable = false, length = 50)
    private String department;
    @Column(nullable = false)
    private Boolean status;
    @Column(nullable = false)
    private Boolean deleted;
    @Column(nullable = false)
    private Date registrationTime;
    @JsonIgnore
    @Column(name = "category_id", nullable = false)
    private Long categoryId;
    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Category category;
    @JsonIgnore
    @Column(nullable = false)
    private long primaryKeyId;


}
